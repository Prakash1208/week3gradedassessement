package packageinventory;

public class Book {
	private String name;
	private double price;
	private String genre;
	private int noOfCopiesSold;
	public Book() {
		super();
	}
	
	public Book(String name, double price, String genre, int noOfCopiesSold) {
		super();
		this.name = name;
		this.price = price;
		this.genre = genre;
		this.noOfCopiesSold = noOfCopiesSold;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public String getGenre() {
		return genre;
	}
	
	public void setGenre(String genre) {
		this.genre = genre;
	}
	
	public int getNoOfCopyesSold() {
		return noOfCopiesSold;
	}
	
	public void setNoOfCopyesSold(int noOfCopiesSold) {
		this.noOfCopiesSold = noOfCopiesSold;
	}
	
	@Override
	public String toString() {
		return "Book [name=" + name + ", price=" + price + ", genre=" + genre + ", noOfCopiesSold=" + noOfCopiesSold+ "]";
	}	

}
